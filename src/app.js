let greeting = document.querySelector(".greeting");
let story = document.querySelector(".background");
let likes = document.querySelector(".likes");
let shortLongName = document.querySelector(".namesLength");
let farewell = document.querySelector("farewell");

let userName = document.querySelector('.userName').value;

setTimeout(function () {
    greeting.style.display = "block";
    document.innerHTML = " Hello, my name is Miriam.";
}, 1000);

setTimeout(function () {
    story.style.display = "block";
    document.innerHTML = "I was an English teacher, but right now I’m studying Web Development.";

}, 2000);

setTimeout(function () {
    likes.style.display = "block";
    document.innerHTML = "I like to watch series. My favorite series is Mr. Robot"
}, 3000);

//here comes the complicated point

setTimeout(function () {
    if (userName !== /^([]{6})$/i.test(userName)) {
        userName.style.display = "block";
        document.innerHTML = "You need to enter your name boss";
    } else if (shortLongName >= 6) {
        document.innerHTML = "That’s a short name.";
    } else {
        document.innerHTML = "That's a long name";
    }

}, 4000);

setTimeout(function () {
    farewell.style.display = "block";
}, 5000);